package User

import (
	"github.com/satori/go.uuid"
	"gorm.io/gorm"
)

type User struct {
	ID         uint      `gorm:"primary_key"`
	Uuid       uuid.UUID `gorm:"sql:index;"`
	Username   string    `gorm:"size:255;not null;unique" json:"username" form:"username"`
	Email      string    `gorm:"size:100;not null;unique" json:"email"`
	Password   string    `gorm:"size:100;not null;" json:"password"`
	gorm.Model `gorm:"af"`
}
