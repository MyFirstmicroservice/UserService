package Request

import "github.com/gookit/validate"

type Request interface {
	GetData() map[string]interface{}
	Role() validate.Errors
}
