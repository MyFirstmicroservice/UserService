package CreateUserRequest

import (
	"github.com/gofiber/fiber/v2"
	"github.com/gookit/validate"
)

type CreateUser struct {
	Data      map[string]interface{}
	validator *validate.Validation
}

func New(c *fiber.Ctx) CreateUser {
	get := CreateUser{}
	err := c.BodyParser(&get.Data)
	if err != nil {
	}
	get.validator = validate.Map(get.Data)
	return get
}
func (R CreateUser) Role() validate.Errors {
	R.validator.StringRule("username", "required|string")
	R.validator.StringRule("email", "required|string")
	R.validator.StringRule("password", "required|string")
	R.validator.Validate()
	return R.validator.Errors
}

func (R CreateUser) GetData() map[string]interface{} {
	return R.Data
}
