package LoginUserRequest

import (
	"github.com/gofiber/fiber/v2"
	"github.com/gookit/validate"
)

type LoginUser struct {
	Data      map[string]interface{}
	validator *validate.Validation
}

func New(c *fiber.Ctx) LoginUser {
	get := LoginUser{}
	err := c.BodyParser(&get.Data)
	if err != nil {
	}
	get.validator = validate.Map(get.Data)
	return get
}
func (R LoginUser) Role() validate.Errors {
	//R.validator.StringRule("uuid", "required|string")
	R.validator.StringRule("email", "required|string")
	R.validator.StringRule("password", "required|string")
	R.validator.Validate()
	return R.validator.Errors
}

func (R LoginUser) GetData() map[string]interface{} {
	return R.Data
}
