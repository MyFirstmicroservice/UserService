package GetUserRequest

import (
	"github.com/gofiber/fiber/v2"
	"github.com/gookit/validate"
)

type GetUser struct {
	Data      map[string]interface{}
	validator *validate.Validation
}

func New(c *fiber.Ctx) GetUser {
	get := GetUser{}
	err := c.BodyParser(&get.Data)
	if err != nil {
	}
	get.validator = validate.Map(get.Data)
	return get
}
func (R GetUser) Role() validate.Errors {
	//R.validator.StringRule("user_id", "required|min:1")
	R.validator.Validate()
	return R.validator.Errors
}

func (R GetUser) GetData() map[string]interface{} {
	return R.Data
}
