package main

import (
	"fmt"

	"github.com/gofiber/fiber/v2"
	"github.com/joho/godotenv"
	"log"
	"os"
	"test/database"
	"test/route"
)

func setupRoute(app *fiber.App) {

	route.SetupRoute(app)
}

func main() {

	app := fiber.New()
	setupRoute(app)
	var _ error

	err := godotenv.Load()
	if err != nil {
		log.Fatalf("Error getting env, not comming through %v", err)
	} else {
		fmt.Println("We are getting the env values")
	}
	database.Initialize("mysql", os.Getenv("DB_USER"), os.Getenv("DB_PASSWORD"), os.Getenv("DB_PORT"), os.Getenv("DB_HOST"), os.Getenv("DB_NAME"))

	err = app.Listen(":4000")
	if err != nil {
		return
	}
}
