package LogFile

import (
	"io"
	"log"
	"os"
)

type Log struct {
	ContentError string
	File         io.Writer
	Logger       *log.Logger
}

func New(Error string) *Log {
	file, _ := os.OpenFile("SystemLog.log", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)
	return &Log{
		ContentError: Error,
		File:         file,
		Logger:       new(log.Logger),
	}
}

func (L *Log) Error() *log.Logger {
	L.Logger.SetOutput(L.File)
	L.Logger.SetFlags(log.Lshortfile | log.Ldate | log.Ltime)
	L.Logger.SetPrefix(L.ContentError)
	return L.Logger
}
