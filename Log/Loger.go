package Log

import "log"

type Log interface {
	Printer() *log.Logger
}
