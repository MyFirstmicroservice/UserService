package RedisRepository

import (
	"fmt"
	"github.com/go-redis/redis"
)

type RedisRepository struct {
	Client *redis.Client
	Prefix string
}

func New(Prefix string) *RedisRepository {
	client := redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "",
		DB:       0,
	})

	return &RedisRepository{
		Client: client,
		Prefix: Prefix,
	}
}

func (Redis *RedisRepository) InsertToRedis(key string, Value interface{}) error {
	err := Redis.Client.Set(key, Value, 0).Err()
	if err != nil {
		return err
	}
	return nil
}

func (Redis *RedisRepository) readFromRedis(Key string) {
	result := Redis.Client.Get(Key)
	if result != nil {
		fmt.Print(result)
	}

}

func existToRedis() {

}
