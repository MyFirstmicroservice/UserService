package UserRepository

import (
	"fmt"
	"github.com/gofiber/fiber/v2"
	"github.com/jinzhu/gorm"
	uuid "github.com/satori/go.uuid"
	"golang.org/x/crypto/bcrypt"
	database "test/database"
	"test/models/User"
)

type UserRepository struct {
	User *User.User
	Db   *gorm.DB
}

type IUserRepository interface {
	GetAllUser() *User.User
	CreateUser(c map[string]interface{}) *User.User
}

func NewUserRepository() *UserRepository {

	return &UserRepository{
		Db: database.DB,
	}
}

func (Service UserRepository) GetUser(c map[string]interface{}) *User.User {
	db := database.DB
	User := new(User.User)
	db.First(&User, "email = ?", c["email"].(string))
	fmt.Printf("%v", &User)
	return User
}

func (Service UserRepository) GetAllUser() *User.User {
	fmt.Println("ss")
	db := database.DB
	User := new(User.User)
	db.Find(&User)

	return User
}
func (Service *UserRepository) CreateUser(c map[string]interface{}) (*User.User, error) {
	db := Service.Db
	user := new(User.User)
	user.Username = c["username"].(string)
	user.Email = c["email"].(string)
	user.Uuid = uuid.NewV1()
	fmt.Printf("name:%v\n", c["email"].(string))
	// Store the body in the user and return error if encountered
	password, err := bcrypt.GenerateFromPassword([]byte(c["password"].(string)), bcrypt.DefaultCost)
	user.Password = string(password)
	fmt.Printf("%v", user.Password)
	// Create the Note and return error if encountered
	err = db.Create(&user).Error
	if err != nil {
		fmt.Printf("error is :%v", err)
	}

	// Return the created user
	return user, err
}

func UpdateUser(c *fiber.Ctx) error {
	type updateUser struct {
		Username string `json:"Username"form:"username"`
		Email    string `json:"email"form:"email"`
	}
	db := database.DB
	note := new(User.User)
	id := c.Params("id")

	// Store the body in the note and return error if encountered
	var updateUserData updateUser
	err := c.BodyParser(&updateUserData)
	if err != nil {
		return c.Status(500).JSON(fiber.Map{"status": "error", "message": "Review your input", "data": err})
	}
	db.Find(&note, "id = ?", id)

	fmt.Println(updateUserData.Email)
	note.Email = updateUserData.Email
	note.Username = updateUserData.Username
	db.Save(&note)
	// Return the created note
	return c.JSON(fiber.Map{"status": "success", "message": "Update Note", "data": note})
}
