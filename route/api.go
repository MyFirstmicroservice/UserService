package route

import (
	"github.com/gofiber/fiber/v2"
	"test/controllers/UserController"
)

func SetupRoute(app *fiber.App) {
	v1 := app.Group("/api")
	v1 = v1.Group("/user")

	v1.Get("", UserController.GetAllUser)
	v1.Post("/login", UserController.Login)
	v1.Post("/register", UserController.CreateUser)
	v1.Put("/:id", UserController.UpdateUser)
}
