package UserController

import (
	"fmt"
	"github.com/gofiber/fiber/v2"
	"log"
	"test/Log"
	"test/Repository/UserRepository"
	"test/Request/CreateUserRequest"
	"test/Request/GetUserRequest"
	"test/Request/LoginUserRequest"
	"test/Service/AuthService/JwtAuthService"
	"test/Service/UserService"
)

func GetUser(c *fiber.Ctx) {
	Request := GetUserRequest.New(c)
	err := Request.Role()
	if len(err) > 0 {
		c.JSON(err)
	} else {
		c.JSON(Request.GetData())
	}

}
func GetAllUser(c *fiber.Ctx) error {

	UserService := UserService.NewUserService()
	data := UserService.GetAllUser()
	if data != nil {
		fmt.Println(&data)
		c.Status(fiber.StatusOK).JSON(fiber.Map{
			"massage": "success",
			"data":    data,
		})
	}
	return nil

}

func Login(c *fiber.Ctx) error {
	_, err := JwtAuthService.GetClaimsFromToken("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJVdWlkIjoiZmI1Zjg2MzktYTE2Ny0xMWVjLTk5NDYtOGMxNjQ1ZGVmNmY4IiwiQXV0aG9yaXplZCI6dHJ1ZSwiVXNlcm5hbWUiOiIifQ.PfV5rl6l4rt2Tfuu3Q8FfxefQJbtqXinK-vI8GeyMUM")
	if err != nil {
		return err
	}
	Request := LoginUserRequest.New(c)
	data := Request.GetData()
	errRequest := Request.Role()
	if len(errRequest) > 0 {
		return c.JSON(fiber.Map{
			"err": errRequest,
		})
	} else {
		UserService := UserService.NewUserService()
		re, err := UserService.Login(data)
		if err != nil {
			return c.JSON(fiber.Map{
				"data":    "password is not match",
				"massage": re,
			})
		}
		AuthService := JwtAuthService.GenerateJWT(re["Uuid"].(string))
		return c.JSON(
			fiber.Map{
				"token": AuthService,
				"data":  re,
			})

	}
	return nil
}

func CreateUser(c *fiber.Ctx) error {
	Request := CreateUserRequest.New(c)
	data := Request.GetData()
	errRequest := Request.Role()
	if len(errRequest) > 0 {
		c.JSON(fiber.Map{
			"err": errRequest,
		})
	} else {
		UserService := UserService.NewUserService()
		re, _ := UserService.CreateUser(data)

		AuthService := JwtAuthService.GenerateJWT(re.Uuid.String())
		c.JSON(fiber.Map{
			"token": AuthService,
			"data":  re,
		})
	}
	return nil
}

func UpdateUser(c *fiber.Ctx) error {
	err := UserRepository.UpdateUser(c)
	if err != nil {
		return nil
	}

	return nil
}

func InsertLog(tempLog Log.Log) *log.Logger {
	return tempLog.Printer()

}
