package JwtAuthService

import (
	"fmt"
	"test/Repository/RedisRepository"
	"test/Repository/UserRepository"
	"time"
)
import (
	//...
	// import the jwt-go library
	"github.com/dgrijalva/jwt-go"
	//...
)

var secret = []byte("test")

// Claims Create a struct that will be encoded to a JWT.
// We add jwt.StandardClaims as an embedded type, to provide fields like expiry time
type Claims struct {
	Uuid       string `json:"Uuid"`
	Authorized bool   `json:"Authorized"`
	Username   string `json:"Username"`
	exp        int64
	jwt.StandardClaims
}

var Redis *RedisRepository.RedisRepository

type JwtAuthService struct {
	UserRepository  *UserRepository.UserRepository
	RedisRepository *RedisRepository.RedisRepository
}

func init() {
	Redis = RedisRepository.New("")
}

//New CreateJwtAuthService
func New() *JwtAuthService {
	return &JwtAuthService{
		UserRepository:  UserRepository.NewUserRepository(),
		RedisRepository: RedisRepository.New(""),
	}
}

func SingUp() {

}

func GenerateJWT(uuid string) string {
	var mySigningKey = []byte("test")

	claims := Claims{
		Uuid:       uuid,
		Authorized: true,
		exp:        time.Now().Add(time.Minute * 30).Unix(),
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	tokenString, err := token.SignedString(mySigningKey)
	if err != nil {
		fmt.Errorf("Something Went Wrong: %s", err.Error())
		return ""
	}
	err = Redis.InsertToRedis(uuid, tokenString)
	if err != nil {
		fmt.Printf("Something Went Wrong: %s", err.Error())
		return ""
	}

	return tokenString
}

func ValidateToken() {

}

func GetClaimsFromToken(tokenString string) (jwt.MapClaims, error) {
	claims := &Claims{}
	token, err := jwt.ParseWithClaims(tokenString, claims, func(token *jwt.Token) (interface{}, error) {
		return secret, nil

	})
	fmt.Printf("UUid:%s", claims.Uuid)

	if err != nil {
		return nil, err
	}
	if claims1, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		return claims1, nil
	}
	return nil, err
}
