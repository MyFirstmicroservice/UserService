package UserService

import (
	"encoding/json"
	"golang.org/x/crypto/bcrypt"
	"test/Repository/UserRepository"
	"test/models/User"
)

type UserService struct {
	UserRepository *UserRepository.UserRepository
}

func NewUserService() *UserService {
	return &UserService{
		UserRepository: UserRepository.NewUserRepository(),
	}
}
func (service *UserService) Login(c map[string]interface{}) (map[string]interface{}, error) {
	passwordinput := c["password"].(string)
	data := service.GetUser(c)
	passwordHashed := data["password"].(string)
	err := bcrypt.CompareHashAndPassword([]byte(passwordHashed), []byte(passwordinput))
	if err != nil {
		return nil, err
	}
	return data, nil
}
func (Service UserService) GetUser(data map[string]interface{}) map[string]interface{} {
	User := Service.UserRepository.GetUser(data)
	temp, _ := json.Marshal(User)
	err := json.Unmarshal(temp, &data)
	if err != nil {
		return nil
	}
	return data
}

func (Service UserService) GetAllUser() map[string]interface{} {
	user := Service.UserRepository.GetAllUser()
	var inInterface map[string]interface{}
	data, _ := json.Marshal(user)
	err := json.Unmarshal(data, &inInterface)
	if err != nil {
		return nil
	}
	return inInterface

}
func (Service UserService) CreateUser(data map[string]interface{}) (*User.User, error) {
	return Service.UserRepository.CreateUser(data)

}
